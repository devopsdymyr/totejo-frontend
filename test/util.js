var childProcess = require("child_process");
var process = require("process");
var getPort = require("get-port");
var wdio = require("webdriverio");
var rp = require("request-promise-native");

var renderer = require("vue-server-renderer").createRenderer();
var jsdom = require("jsdom");
var JSPM = require("jspm");
var tape = require("tape");

var USER_FIXTURES = require("./fixtures/users");

var PROCESSES_TO_KILL = [];
var WAIT_BEFORE_KILL_AND_EXIT_MS = 500;

process.on("SIGINT", () => PROCESSES_TO_KILL.forEach(p => p.kill()));
process.on("exit", () => PROCESSES_TO_KILL.forEach(p => p.kill()));

tape.onFinish(() => {
  setTimeout(() => { // nexttick is not enough
    PROCESSES_TO_KILL.forEach(p => p.kill());
  }, WAIT_BEFORE_KILL_AND_EXIT_MS);
});
exports.tape = tape;

const defaultTestConfig = {timeout: 10000};
exports.defaultTestConfig = defaultTestConfig;

/**
 * Render a component with data provided
 *
 * @param {Object} Vue - The vue instance to use to render the component
 * @param {Object} Component - The component to render
 * @param {Object} props - The data to feed in to the component at init
 * @returns A promise that resolves to the `window` element of a DOM
 */
exports.renderComponentWithData = function(Vue, Component, props) {
  // Create component and mount
  var ModifiedVue = Vue.extend(Component);
  var vm = new ModifiedVue(props || {});

  return new Promise(function(resolve, reject) {
    // Render the component
    renderer.renderToString(vm, function(err, html) {
      if (err) reject(err);
      resolve(new jsdom.JSDOM(html).window);
    });
  });
};

/**
 * Render a module (usually a component) in JSDOM, by:
 * - Creating a JSPM builder
 * - Building a static bundle of the module
 * - Loading the bundle with JSDOM
 *
 * @param {string} path - Path to the module code
 * @param {string} name - The name to use for the name of the exported module
 * @param {string} [builder] - JSPM builder to re-use
 * @returns A promise that resolves with the window of the generated jsdom environment
 */
function renderModuleInJSDOM(path, name, builder) {
  // Make a static build of the module
  return (builder || new JSPM.Builder())
    .buildStatic(path, { minify: true, globalName: "window." + name })
    .then(output => new jsdom.JSDOM(`<script>${output.source}</script>`, {runScripts: "dangerously"}).window);
}
exports.renderModuleInJSDOM = renderModuleInJSDOM;

/**
 * Instantiate a component into an off-page element from a loaded global module in JSDOM
 * The component module is expected to have `vue` and `component` properties.
 *
 * @param {string} globalName - The name of the loaded component module.
 * @param {Object} data - Data to pass to the component on initialization
 * @param {Object} data.propsData - Initial props for the component
 * @returns A function that operates on a window, and resolves to original window, the vm, and element or the relevant component
 */
function instantiateComponentFromLoadedGlobal(globalName, data) {
  return function(window) {
    if (typeof window[globalName] === "undefined" || window[globalName] == null) {
      throw new Error("Incorrect/Invalid global name, failed to find window[" + globalName + "]");
    }

    // Grab the loaded element, component, and vue fn
    var vueFn  = window[globalName].default.vue;
    var comp  = vueFn.extend(window[globalName].default.component);

    // Create and mount component into off-page element
    var vm = (new comp(data)).$mount();
    var elem = vm.$el;

    // NOTE - the element is NOT on the page at this point, it"s off-page
    // The structure below is referred to as an ElemInfo object frequently.
    return {vm, elem, vueFn, comp, window};
  };
}

exports.instantiateComponentFromLoadedGlobal = instantiateComponentFromLoadedGlobal;

function isValidElemInfoObj(obj) {
  return obj.vm && obj.elem && obj.window && obj.comp;
}

/**
 * Add component"s element (expeted to be off-page) to the body of the HTML document. Usually called after `instantiateComponentFromLoadedGlobal`
 *
 * @param {ElemInfo} info - Information about element/env see `instantiateComponentFromLoadedGlobal`
 * @returns A an object (elemInfo) that contains the window, the vm, and element or the relevant component. see `instantiateComponentFromLoadedGlobal`
 */
function addComponentElementToBody(info) {
  if (!isValidElemInfoObj(info)) throw new Error("Invalid ElemInfo object:", info);

  // Add elem to document body
  info.window
    .document
    .documentElement
    .getElementsByTagName("body")[0]
    .appendChild(info.elem);

  // NOTE - the element is NOT on the page at this point, it"s off-page
  return info;
}

exports.addComponentElementToBody = addComponentElementToBody;

/**
 * Simulate a click in JSDOM
 *
 * @param {EventTarget} tgt - The element that should be the target for the click
 * @param {Window} window - The JSDOM-provided window of the page
 * @param {object} options - Click event options (by default an cancelable, event that bubbles, with the given element as the view)
 * @param {Function} tgt.dispatchEvent - The element should be able to dispatch events.
 */
function jsdomClick(tgt, window, options) {
  tgt.dispatchEvent(
    new window.MouseEvent("click", options || { bubbles: true, cancelable: true, view: tgt })
  );
}
exports.jsdomClick = jsdomClick;

/**
 * Fail and end a test as provided by tape.
 *
 * @param {Object} t - Test to fail and end
 * @param {Function} t.fail - Should cause a failing assertion
 * @param {Function} t.fail - Should cause a failing assertion
 * @param {string} msg - Failure message
 * @returns A function that ignores it"s input, calls .fail then .end on the given t
 */
function failAndEnd(t, msg) {
  return function(err) {
    t.fail(msg || "failAndEnd called with no message");
    t.end(err);
  };
}
exports.failAndEnd = failAndEnd;

exports.tape = tape;

/**
 * Run some code in the context of a running app instance
 *
 * @param {Function} tape - `test` function provided by Tape
 * @param {string} name - The name of the child process that is going to be created
 * @param {Object} options - Options to use with the running app instance
 * @param {String|Function} options.cmd - Command to spawn in a child process
 * @param {Object} [options.process] - Object containing config to use for the spawned child process
 * @param {Object|Function} [options.process.env] - ENV variables that will be passed to the started process
 * @param {Function} fn - The function to run with the app instance. This function is a passed an object with shape {info: {...}, process: ChildProcess }, with a possibly present error. If the function returns a promise, the child process will be alive as long as the promise is alive.
 * @returns a Promise that resolves to an object containing information about the running child process
 */
var startChildProcess = (tape, name, options) => {
  return new Promise(resolve => {
    (options.port ? Promise.resolve(options.port) : getPort())
      .then(function(port) {
        options.port = port;

        // Generate appropriate ENV to be passed to child process
        if (options.process &&
            options.process.env &&
            typeof options.process.env === "function") {
          options.process.env = options.process.env(options);
        }

        // Generate the data that will be passed back to the
        if (typeof options.info === "function") { options.info = options.info(options); }

        // Generate the command, if a function was provided
        if (typeof options.cmd === "function") { options.cmd = options.cmd(options); }

        // Spawn child process, keep a pointer to it to clean it up later
        var generatedProcess = childProcess.spawn(
          options.cmd.command,
          options.cmd.args,
          {env: options.process.env}
        );
        PROCESSES_TO_KILL.push(generatedProcess);

        // Log output of childprocess if specified
        if (options.logStdout) { generatedProcess.stdout.on("data", d => tape.comment(`${name}.stdout: ${d}`)); }
        if (options.logStderr) { generatedProcess.stderr.on("data", d => tape.comment(`${name}.stderr: ${d}`)); }

        tape.comment(`Process ${name} started on port [${options.port}]`);

        setTimeout(() => resolve({info: options.info, process: generatedProcess, cleanup: () => generatedProcess.kill}), options.afterStartDelayMs || 0);
      });
  });
};

/**
 * Run the provided function with an app instance running in a child process.
 *
 * @param {Function} fn - The function to run (likely containing tests/assertions). This function should expect an objects with shape {info: {...}, process: ChildProcess}, along with a possibly present error.
 */
exports.startTestAppInstance = tape => startChildProcess(tape, "App", {
  afterStartDelayMs: 500,
  process: {
    env: (options) => {
      if (!process.env.DB_MIGRATION_FOLDER) { throw new Error("DB_MIGRATION_FOLDER ENV variable missing!"); }
      if (!process.env.FRONTEND_FOLDER) { throw new Error("FRONTEND_FOLDER ENV variable missing!"); }

      return {
        ENVIRONMENT: "Test",
        DB_MIGRATION_FOLDER: process.env.DB_MIGRATION_FOLDER,
        FRONTEND_FOLDER: process.env.FRONTEND_FOLDER,
        PORT: options.port
      };
    }
  },

  info: options => {
    if (!process.env.TEST_APP_BIN_PATH) { throw new Error("TEST_APP_BIN_PATH ENV variable missing!"); }

    return Object.assign({
      binPath: process.env.TEST_APP_BIN_PATH,
      appUrl: `http://localhost:${options.port}`
    }, options);
  },

  cmd: options => ({command: options.info.binPath, args: ["RunServer"]})
});

/**
 * Executes a function in a context in which a chromedriver browser session has been made
 *
 * @param {Function} tape - The tape (`test`) function
 * @param {Promise} chromeDriverInfoPromise - A promise that evaluates to information about the chromedriver
 * @param {Function} fn - The function to execute
 * @returns A promise that resolves to the result of the function execution with a browser passed into it
 */
const startNewChromeDriverInstance = tape => {
  return new Promise(resolve => {
    return getPort()
      .then(port => {

        var process = childProcess.spawn("chromedriver", [
          "--url-base=wd/hub",
          `--port=${port}`
        ]);
        PROCESSES_TO_KILL.push(process);

        process.stdout.on("data", d => {
          if (`${d}`.match(/local connections are allowed/)) {
            tape.comment(`Chromedriver expected to be running at localhost:${port}`);

            // Setup browser
            var browser = wdio.remote({
              port,
              desiredCapabilities: {
                browserName: "chrome",
                chromeOptions: {
                  args: [ "--headless"]
                }
              }
            });
            browser.on("error", tape.fail);

            resolve({browser, cleanup: () => browser.end()});
          }
        });
      });
  });
};
exports.startNewChromeDriverInstance = startNewChromeDriverInstance;

// Check an assertion and return the browser object
const assertAndReturnBrowser = (tapeTestFn, browser, fn, msg) => {
  return function() {
    tapeTestFn.assert(fn(...arguments), msg);
    return browser;
  };
};
exports.assertAndReturnBrowser = assertAndReturnBrowser;

const ROUTE_FUNCTIONS = {
  // App endpoints
  login: base => `${base}/#/login`,
  app: base => `${base}/#/app`,
  admin: base => `${base}/#/app/admin`,
  manageCompanies: base => `${base}/#/app/admin/companies`,

  // API endpoints
  apiLogin: base => `${base}/api/v1/login`,
  apiUsers: base => `${base}/api/v1/users`,
  apiJobPostingRequests: base => `${base}/api/v1/jobs/postings/requests`,
  apiApproveJobPostingRequest: (base, rid) => `${base}/api/v1/jobs/postings/requests/${rid}/approve`,
  apiCompanies: base => `${base}/api/v1/companies`
};

const makeRouteURL = (route) => ROUTE_FUNCTIONS[route];
exports.makeRouteURL = makeRouteURL;

const doLogin = (tape, browser, appBaseURL, email, pass) => {
  return browser
    .then(() => {
      return browser
        .url(makeRouteURL("login")(appBaseURL))
        .isExisting("input#email")
        .then(assertAndReturnBrowser(tape, browser, it => it.should.be.true("email input rendered")))
        .isExisting("input#password")
        .then(assertAndReturnBrowser(tape, browser, it => it.should.be.true("password input rendered")))
        .isExisting("button#btn-login")
        .then(assertAndReturnBrowser(tape, browser, it => it.should.be.true("login button rendered")))
        .then(() => browser.setValue("input#email", email))
        .then(() => browser.getValue("input#email"))
        .then(assertAndReturnBrowser(tape, browser, it => it.should.be.eql(email, "email should match")))
        .then(() => browser.setValue("input#password", pass))
        .then(() => browser.getValue("input#password"))
        .then(assertAndReturnBrowser(tape, browser, it => it.should.be.eql(pass, "password should match")))
        .click("button#btn-login")
        .pause(1000);
    });
};
exports.doLogin = doLogin;
exports.doLoginForUser = (tape, browser, appBaseURL, u) => () => doLogin(tape, browser, appBaseURL, u.emailAddress, u.password);

/**
 * Do logins and get an auth cookie
 *
 * @param {Function} tape - The `test` function
 * @param {String} appBaseURL - base URL for the app
 * @param {String} email
 * @param {String} pass
 * @returns A promise that resolves to the auth cookie value
 */
const doAPICookieLogin = (tape, appBaseURL, email, pass) => {
  var requestOptions = {
    method: "POST",
    url: makeRouteURL("apiLogin")(appBaseURL),
    body: {userEmail: email, userPassword: pass},
    resolveWithFullResponse: true,
    json: true
  };

  return rp(requestOptions)
    .then(resp => {
      if (resp.body.status !== "success") { throw new Error(`API Login to url [${requestOptions.url}] failed with message: [${resp.body.message}]`); }
      tape.ok(resp.body, "api login response body is valid");
      var results = /job-board-auth=([^;]+);/.exec(resp.headers["set-cookie"]);
      tape.assert(results.length > 0, "cookies were returned with the 'job-board-auth' name");
      return results[1];
    });
};
exports.doAPICookieLogin = doAPICookieLogin;
exports.doAPICookieLoginForUser = (tape, appBaseURL, u) => () => doAPICookieLogin(tape, appBaseURL, u.emailAddress, u.password);

/**
   * Create a backend entity by posting to a URL
   *
   * @param {Function} tape - `test` function provided by tape
   * @param {Object} opts - options
   * @param {string} [opts.authCookie] - Cookie value will be included in the request if provided, with name "job-board-auth"
   * @param {string} opts.body - Request body
   * @returns a Promise that resolves to the server's response
   */
const makeEntity = (tape, url, opts) => {
  var requestOptions = {
    method: "POST",
    url: url,
    body: opts.body,
    json: true
  };

  // Add cookie auth if provided
  if (opts.authCookie) {
    var j = rp.jar();
    j.setCookie(rp.cookie(`job-board-auth=${opts.authCookie}`), url);
    requestOptions.jar = j;
  }

  return rp(requestOptions)
    .then(({status, message, respData}) => {
      if (status !== "success") { throw new Error(`Entity creation using URL [${url}] failed with message: [${message}]`); }
      tape.ok(respData, "returned entity is valid");
      return respData;
    })
    .catch(tape.end);
};

const getReq =  (tape, url, opts) => {
  var requestOptions = {url: url, headers: {}, json: true};

  // Add cookie auth if provided
  if (opts.authCookie) {
    var j = rp.jar();
    j.setCookie(rp.cookie(`job-board-auth=${opts.authCookie}`), url);
    requestOptions.jar = j;
  }

  return rp(requestOptions)
    .then(({status, message, respData}) => {
      if (status !== "success") { throw new Error(`Entity creation using URL [${url}] failed with message: [${message}]`); }
      tape.ok(respData, "returned entity is valid");
      return respData;
    })
    .catch(tape.end);
};

const makeUser = (tape, baseUrl) => opts => makeEntity(tape, makeRouteURL("apiUsers")(baseUrl), opts);
exports.makeAdminUser = (tape, baseUrl) => makeUser(tape, baseUrl)({body: USER_FIXTURES.ADMIN});
exports.getUsers = (tape, baseUrl, opts) => getReq(tape, makeRouteURL("apiUsers")(baseUrl), opts);

exports.makeCompany = (tape, baseUrl, opts) => makeEntity(tape, makeRouteURL("apiCompanies")(baseUrl), opts);
exports.makeJobPostingRequest = (tape, baseUrl, opts) => makeEntity(tape, makeRouteURL("apiJobPostingRequests")(baseUrl), opts);

exports.approveJobPostingRequest = (tape, baseUrl, jprId, opts) => getReq(tape, makeRouteURL("apiApproveJobPostingRequest")(baseUrl, jprId), opts);

exports.chromeDriverSetupTest = tape => {
  var promiseResolver;
  var chromeDriverPromise = new Promise((resolve) => promiseResolver = resolve);

  tape("set up chromedriver", defaultTestConfig, t => {
    startNewChromeDriverInstance(t)
      .then(promiseResolver)
      .then(() => t.end(), t.end);
  });

  return chromeDriverPromise;
};

exports.chromeDriverCleanupTest = (tape, driverPromise) => tape("teardown chromedriver", defaultTestConfig, t => {
  driverPromise
    .then(d => d.cleanup())
    .then(() => t.end(), t.end);
});
