exports.makeDevJobPostingRequest = companyId => ({
  rTitle: "[Test] Developer job",
  rDescription: "This is a test job",
  rIndustry: "IT",
  rJobType: "FullTime",
  rPosterEmail: "hr@example.com",
  rApplyLink: "hr@example.com.",
  rCompanyId: companyId,
  rRequestedAt: "2017-07-11T08:30:17.725Z"
});
