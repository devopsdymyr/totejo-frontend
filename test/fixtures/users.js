exports.ADMIN = {
  emailAddress: "test@example.com",
  firstName: "Admin",
  lastName: "User",
  password: "test",
  userRole: "Administrator",
  joinedAt: "2017-07-11T08:30:17.725Z",
  homeLocationId: 2 // Japan
};
