exports.STARTUP = {
  companyName: "[Test] Startup",
  companyDescription: "This is a test company",
  companyCultureDescription: "This is a great company with cool work",
  companyHomepageURL: "example.com",
  companyIconURI: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=", // Starts as Data URI
  companyBannerImageURI: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=", // Starts as Data URI
  companyPrimaryColorCSS: "gray", //    :: String
  companyCreatedAt: "2017-07-11T08:30:17.725Z",
  companyLastUpdatedAt: "2017-07-11T08:30:17.725Z",
  companyAddressID: null
};
