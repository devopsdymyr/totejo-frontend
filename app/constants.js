const URLS = {
  SERVER_BASE_URL: "",
  API_BASE: "api",
  API_VERSION: "v1",
  LOGIN: "login",
  LOGOUT: "logout",
  USERS: "users",

  JOBS: "jobs",
  JOBS_SEARCH: "jobs/search",
  JOBS_POSTINGS_REQUESTS: "jobs/postings/requests",
  URL_BOUNCE_CONFIGS: "url-bounce-configs",
  PERMISSIONS: "permissions",
  INDUSTRIES: "industries",
  ADMIN_STATISTICS: "admin/statistics",

  COMPANIES: "companies",
  TAGS: "tags",
  ADDRESSES: "addresses",

  ME: "me",
  STARRED_JOBS: "starred-jobs",
  CHANGE_PASSWORD: "change-password",
  SUPPORT_REQUESTS: "support-requests",
  SUBSCRIPTIONS: "subscriptions",
};

URLS.API_BASE_URL = `${URLS.SERVER_BASE_URL}/${URLS.API_BASE}/${URLS.API_VERSION}`;

const ROLES = {
  ADMINISTRATOR: "Administrator",
  JOB_SEEKER: "JobSeeker",
  COMPANY_REPRESENTATIVE: "CompanyRepresentative"
};

const JOB_TYPES = {
  FULL_TIME: "FullTime",
  PART_TIME: "PartTime",
  FREELANCE: "Freelance",
  CONTRACT: "Contract"
};

const SUPPORT_REQUEST_TYPES = {
  GENERAL: "General"
};

const ENUM_TRANSLATIONS = {
  currency: {
    "YEN": "Japanese Yen (￥)",
    "USD": "US Dollar ($)"
  },

  locationByID: {
    1: "United States",
    2: "Japan"
  },

  industry: {
    "IT": "Information Technology (IT)",
    "BizDev": "Business Development"
  },

  job: {
    type: {
      "FullTime": "Full Time",
      "PartTime": "Part Time",
      "Freelance": "Freelance",
      "Contract": "Contract"
    }
  },

  permissions: {
    "ManageJobPostings": "Manage job postings"
  },

  supportRequest: {
    type: {
      "General": "General"
    }
  },

  user: {
    role: {
      "Administrator": "Administrator",
      "JobSeeker": "Job Seeker",
      "CompanyRepresentative": "Company Representative"
    }
  }

};

// Throttle most requests by 3 seconds by default
const DEFAULT_REQUEST_THROTTLE_MS = 3000;

export default {
  URLS,
  ROLES,
  JOB_TYPES,
  SUPPORT_REQUEST_TYPES,
  ENUM_TRANSLATIONS,
  DEFAULT_REQUEST_THROTTLE_MS
};
