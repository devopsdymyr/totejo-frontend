import _ from "lodash";
import Vue from "app/vue";
import MailingListService from "app/services/mailing-list";
import AlertService from "app/services/alert";
import AppService from "app/services/app";

import t from "./template.html!vtc";

import "mrman/vue-component-library/components/alert-listing/component";
import "mrman/vue-component-library/components/async-button/component";

const PAGE_ACTIONS = {
  CONFIRM_SUBSCRIPTION: "confirm-subscription",
  UNSUBSCRIBE: "unsubscribe"
};

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    alertSvc: {type: Object, default: () => AlertService },
    appSvc: {type: Object, default: () => AppService },
    mailingListSvc: {type: Object, default: () => MailingListService },
    token: {type: String, required: true },
    action: {type: String, default: PAGE_ACTIONS.CONFIRM_SUBSCRIPTION }
  },

  beforeMount: function() {
    // Redirect if no token was provided
    if (!this.token || _.isEmpty(this.token)) {
      this.appSvc.doNavigation(r => r.push("/"));
    }
  },

  data: function() {
    return {
      alerts: this.alertSvc.getAlertsForNamespace("mailing-list-subscription-mgmt"),
      actions: PAGE_ACTIONS,
    };
  },

  methods: {
    unsubscribe: function() {
      return this.mailingListSvc
        .unsubscribe(this.token)
        .then(() => {
          this.alertSvc.addAlertForNamespace({
            status: "success",
            closable: false,
            message: `Successfully unsubscribed. You should stop receiveing emails from ${this.appSvc.state.appDomain} immediately.`
          }, "mailing-list-subscription-mgmt");
        })
        .catch(() => {
          this.alertSvc.addAlertForNamespace({
            status: "error",
            closable: false,
            message: `Failed to unsubscribe from mailing list. Please contact support@${this.appSvc.state.appDomain} for help.`
          }, "mailing-list-subscription-mgmt");
        });
    },

    confirmSubscription: function() {
      return this.mailingListSvc
        .confirmSubscription(this.token)
        .then(() => {
          this.alertSvc.addAlertForNamespace({
            status: "success",
            closable: false,
            message: `Subscription confirmed. Thanks for subscribing to ${this.appSvc.state.appDomain}!`
          }, "mailing-list-subscription-mgmt");
        })
        .catch(() => {
          this.alertSvc.addAlertForNamespace({
            status: "error",
            closable: false,
            message: `Failed to confirm mailing list subscription. Please contact support@${this.appSvc.state.appDomain} for help.`
          }, "mailing-list-subscription-mgmt");
        });
    }

  }
};

// Register and export component
Vue.component("mailing-list-subscription-mgmt-page", comp);
export default comp;
