var should = require("should"); // eslint-disable-line no-unused-vars
var TestUtil = require("../../../test/util");
var test = TestUtil.tape;

var COMPONENT_JS_PATH = "app/components/job-search-box/component.js";

test("job-search-box component should render properly with no options", t => {
  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component"))
    .then(elemInfo => {
      t.assert( elemInfo.vm , "VM should be load properly in JSDOM");
      t.end();
    })
    .catch(TestUtil.failAndEnd(t));
});
