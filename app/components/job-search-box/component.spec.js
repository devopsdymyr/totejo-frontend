var should = require("should"); // eslint-disable-line no-unused-vars
var TestUtil = require("../../../test/util");
var JSPM = require("jspm");
var test = TestUtil.tape;

// Define the systemjs path to the component code
var COMPONENT_JS_PATH = "app/components/job-search-box/component.js";

var Component;

test("job-search-box component", t => {

  // Import the component, using JSPM for uinit tests
  JSPM.import(COMPONENT_JS_PATH)
    .then(module => {
      t.ok(module.default.vue);
      t.ok(module.default.component);

      // Save Vue and component class to test-globals for later
      Component = module.default.component;
    })
    .then(t.end)
    .catch(t.fail);
});

test("job-search-box component data is a function", t => {
  t.plan(1);
  t.assert( Component.data.should.have.type("function"), "Component data should be a function");
});

test("job-search-box component data spits out the correct fields and default values", t => {
  t.plan(1);

  var data = Component.data();
  t.assert( data.should.have.type("object"),  "Data function should return an object");
});
