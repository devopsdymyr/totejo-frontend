import _ from "lodash";
import Vue from "app/vue";
import Constants from "app/constants";

import t from "./template.html!vtc";
import "./style.css!";

const ROLE_LOOKUP = {
  JOB_SEEKER: Constants.ROLES.JOB_SEEKER,
  COMPANY_REPRESENTATIVE: Constants.ROLES.COMPANY_REPRESENTATIVE
};

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    initialRole: String
  },

  data: function() {
    var role = this.initialRole || ROLE_LOOKUP.JOB_SEEKER;
    this.$emit("change-role", role);
    return {role};
  },
  computed: {
    roleIsJobSeeker: function() { return this.role === Constants.ROLES.JOB_SEEKER; },
    roleIsCompanyRep: function() { return this.role === Constants.ROLES.COMPANY_REPRESENTATIVE; }
  },

  methods: {
    changeRole: function(r){
      if (!_.has(ROLE_LOOKUP, r)) { throw new Error("Invalid role"); }
      this.role = ROLE_LOOKUP[r];
      this.$emit("change-role", this.role);
    }
  }

};

// Register as global component & export
Vue.component("user-role-select", comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };
