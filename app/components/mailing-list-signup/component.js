import Vue from "app/vue";

import t from "./template.html!vtc";

// https://emailregex.com/
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line no-useless-escape

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    initialEmail: String,
    tags: {type: Array},

    mailingListSvc: {type: Object, required: true},
    alertSvc: {type: Object, required: true}
  },

  data: function() {
    return {
      mailingListAlerts: this.alertSvc.getAlertsForNamespace("mailing-list-signup"),
      formDisabled: false,
      email: this.initialEmail || ""
    };

  },

  computed: {
    trimmedEmail: function() { return this.email.trim(); },
    emailIsInvalid: function() { return !EMAIL_REGEX.exec(this.trimmedEmail); }
  },

  methods: {

    signup: function() {
      // Ensure email is provided
      if (this.emailIsInvalid) {
        this.alertSvc.addAlertForNamespace({
          status: "error",
          message: "Please ensure that the email address you entered is correct, and a valid email address."
        }, "mailing-list-signup");
        return;
      }

      const tagIds = this.tags ? this.tags.map(t => t.id) : [];

      // Sign up using the mailing list service
      this.mailingListSvc
        .subscribe(this.email, tagIds)
        .then(() => {
          this.alertSvc.addAlertForNamespace({
            status: "success",
            message: "Successfully added address to mailing list, please check your inbox for a subscription confirmation."
          }, "mailing-list-signup");

          this.formDisabled = true;
        })
        .catch(() => {
          this.alertSvc.addAlertForNamespace({
            status: "error",
            message: "Failed to add the given email to our mailing list, please try again later."
          }, "mailing-list-signup");
        });
    }

  }

};

// Register as global component & export
Vue.component("mailing-list-signup", comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };
