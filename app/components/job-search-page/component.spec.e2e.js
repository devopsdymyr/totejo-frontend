var should = require("should"); // eslint-disable-line no-unused-vars
var T = require("../../../test/util");
var USER_FIXTURES = require("../../../test/fixtures/users");
var JPR_FIXTURES = require("../../../test/fixtures/job-posting-requests");
var COMPANY_FIXTURES = require("../../../test/fixtures/companies");
var test = T.tape;

const CHROMEDRIVER_PROMISE = T.chromeDriverSetupTest(test);

test("job search page component renders on homepage", T.defaultTestConfig, t => {

  Promise
    .all([T.startTestAppInstance(t), CHROMEDRIVER_PROMISE])
    .then(([app, driver]) => driver.browser.then(() => { // Wait for browser to load
      driver.browser
        .init()
        .url(app.info.appUrl)
        .isExisting("section.job-search-page-component")
        .then(T.assertAndReturnBrowser(t, driver.browser, it => it.should.be.true("component should be present")))
        .then(() => app.cleanup())
        .then(() => driver.cleanup())
        .then(() => t.end(), t.end);
    }))
    .catch(t.end);

});

test("job search page component renders job posting call to action", T.defaultTestConfig, t => {

  Promise
    .all([T.startTestAppInstance(t), CHROMEDRIVER_PROMISE])
    .then(([app, driver]) => driver.browser.then(() => { // Wait for browser to load
      driver.browser
        .init()
        .url(app.info.appUrl)
        .isExisting("span#call-to-action-job-post")
        .then(T.assertAndReturnBrowser(t, driver.browser, it => it.should.be.true("call to action is present")))
        .then(() => app.cleanup())
        .then(() => driver.cleanup())
        .then(() => t.end(), t.end);
    }))
    .catch(t.end);

});

test("job search page component shows jobs from the backend", T.defaultTestConfig, t => {

  Promise
    .all([T.startTestAppInstance(t), CHROMEDRIVER_PROMISE])
    .then(([app, driver]) => driver.browser.then(() => { // Wait for browser to load
      var authCookie;
      driver.browser
        .init()

      // Test setup: make an admin user, a company, and a job posting request, and approve the job posting request
        .then(() => T.makeAdminUser(t, app.info.appUrl))
        .then(T.doAPICookieLoginForUser(t, app.info.appUrl, USER_FIXTURES.ADMIN))
        .then(cookie => authCookie = cookie)
        .then(() => T.makeCompany(t, app.info.appUrl, {body: COMPANY_FIXTURES.STARTUP, authCookie}))
        .then(c => T.makeJobPostingRequest(t, app.info.appUrl, {body: JPR_FIXTURES.makeDevJobPostingRequest(c.id), authCookie}))
        .then(jpr => T.approveJobPostingRequest(t, app.info.appUrl, jpr.id, {authCookie}))

      // Login and refresh job search (default after login with no redirect) job search page, ensure that one job is shown
        .then(T.doLoginForUser(t, driver.browser, app.info.appUrl, USER_FIXTURES.ADMIN))
        .then(() => driver.browser.refresh())
        .then(() => driver.browser.elements(".job-listing-item-component"))
        .then(({value: elements}) => t.assert( elements.should.have.length(1) , "one job posting is present"))

      // Cleanup
        .then(() => app.cleanup())
        .then(() => driver.cleanup())
        .then(() => t.end(), t.end);
    }))
    .catch(t.end);

});

T.chromeDriverCleanupTest(test, CHROMEDRIVER_PROMISE);
