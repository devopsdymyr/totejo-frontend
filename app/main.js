import Vue from "app/vue";
import VueRouter from "vue-router";

import AppService from "app/services/app";
import UserService from "app/services/user";
import LoadingPageIndicatorService from "app/services/page-loading-indicator";

import "app/components/job-posting-request-page/component";
import "app/components/job-search-page/component";
import "app/components/signup-page/component";
import "app/components/mailing-list-subscription-mgmt-page/component";

import "mrman/vue-component-library/components/loading-indicator/component";

import "purecss/build/pure-min.css!";
import "purecss/build/grids-responsive-min.css!";
import "static/fonts/font-awesome-4.7.0/css/font-awesome.min.css!";

import t from "./main.html!vtc";
import "./main.css!";

// Setup router
var router = new VueRouter({
  routes: [
    // Signup page
    { name : "signup", path: "/signup", component: Vue.component("signup-page")},

    // Job request page
    { name : "post-job", path: "/post-job", component: Vue.component("job-posting-request-page")},

    // Job search page
    {
      name: "jobs-search",
      path: "/jobs/search",
      component: Vue.component("job-search-page"),
      props: (r) => ({
        searchTerm: r.query.searchTerm,
        industries: r.query["industries[]"],
        companies: r.query["companies[]"],
        tags: r.query["tags[]"]
      })
    },

    // Mailing list pages
    { name : "mailing-list-subscription-mgmt",
      path: "/mailing-list",
      component: Vue.component("mailing-list-subscription-mgmt-page"),
      props: r => ({
        token: r.query.token,
        action: r.query.action
      })
    },

    // Catch-all for any route that doesn't match, redirects to jobs search page
    { path: "*", redirect: {name: "jobs-search"} }
  ]
});

// Update AppService with the global router
AppService.setPageLoadRoute(window.location.hash.slice(1));
AppService.setRouter(router);

// Create the app vue instance
var app = new Vue({
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  data: {
    loadingSvc: LoadingPageIndicatorService,
    userSvc: UserService,
    appSvc: AppService
  },

  router
}).$mount("#app");

if (window) {
  window.App = app;
}
