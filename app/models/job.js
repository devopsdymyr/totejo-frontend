import Tag from "./tag";

export default class Job {
  constructor(data={}) {
    this.id = data.id;
    this.title = data.title;
    this.description = data.description;
    this.industry = data.industry;
    this.type = data.jobType || data.type;
    this.minSalary = data.minSalary;
    this.maxSalary = data.maxSalary;
    this.salaryCurrency = data.salaryCurrency;
    this.postingDate = data.postingDate;
    this.employerId = data.employerId;
    this.isActive = data.isActive;
    this.applyLink = data.applyLink;
    this.tags = (data.tags || []).map(t => new Tag(t));
    this.jobURLBounceConfigId = data.jobURLBounceConfigId;
  }

  serialize() {
    return {
      id:  this.id,
      title:  this.title,
      description:  this.description,
      industry:  this.industry,
      jobType:  this.type,
      minSalary:  this.minSalary,
      maxSalary:  this.maxSalary,
      salaryCurrency:  this.salaryCurrency,
      postingDate:  this.postingDate,
      employerId:  this.employerId,
      isActive:  this.isActive,
      applyLink:  this.applyLink,
      tags:  this.tags.map(t => t.serialize()),
      jobURLBounceConfigId: this.jobURLBounceConfigId
    };
  }
}
