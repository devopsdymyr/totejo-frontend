class JobPostingRequest {
  constructor(data={}) {
    this.id = data.id;
    this.posterEmail = data.rPosterEmail || data.posterEmail;
    this.title = data.rTitle || data.title;
    this.description = data.rDescription || data.description;
    this.requestedAt = data.rRequestedAt || data.requestedAt;
    this.minSalary = data.rMinSalary || data.minSalary;
    this.maxSalary = data.rMaxSalary || data.maxSalary;
    this.salaryCurrency = data.rSalaryCurrency || data.salaryCurrency;
    this.applyLink = data.rApplyLink || data.applyLink;
    this.companyId = data.rCompanyId || data.companyId;
    this.jobType = data.rJobType || data.jobType;
    this.industry = data.rIndustry || data.industry;
    this.approvedJobId = data.rApprovedJobId || data.approvedJobId;
  }
}

export default JobPostingRequest;
