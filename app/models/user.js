/**
 * Model to  smoothe over UserSessions/UserInfo/UserWithoutSensitiveData objects from backend
 * @class User
 */
class User {
  constructor(data={}) {
    this.id = data.sessionUserID || data.userID || data.id;
    this.firstName = data.userFirstName || data.firstName;
    this.lastName = data.userLastName || data.lastName;
    this.email = data.sessionUserEmail || data.userEmailAddress || data.email;
    this.role = data.userRole || data.role;
    this.joinedAt = data.userJoinedAt || data.joinedAt;
    this.homeLocationId = data.userHomeLocationId || data.homeLocationId;
  }

  serialize() {
    return {
      id: this.id,
      firstName: this.firstName,
      lastName: this.lastName,
      emailAddress: this.email,
      policyAndSalt: null,
      userRole: this.role,
      joinedAt: this.joinedAt,
      homeLocationId: this.homeLocationId
    };

  }
}

export default User;
