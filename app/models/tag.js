export default class Tag {
  constructor(data={}) {
    this.id = data.id;
    this.name = data.tagName || data.name;
    this.names = data.tagNames || data.names;
    this.cssColor = data.tagCSSColor || data.cssColor;
    this.createdAt = data.tagCreatedAt || data.createdAt || new Date();
  }

  serialize() {
    return {
      tagName: this.name,
      tagNames: this.names,
      tagCSSColor: this.cssColor,
      tagCreatedAt: this.createdAt
    };
  }
}
