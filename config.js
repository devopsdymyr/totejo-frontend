System.config({
  baseURL: "./",
  defaultJSExtensions: true,
  transpiler: "babel",
  babelOptions: {
    "optional": [
      "runtime",
      "optimisation.modules.system"
    ]
  },
  paths: {
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*",
    "gitlab:*": "jspm_packages/gitlab/*"
  },
  rootURL: "/",

  map: {
    "babel": "npm:babel-core@5.8.38",
    "babel-runtime": "npm:babel-runtime@5.8.38",
    "core-js": "npm:core-js@1.2.7",
    "css": "github:systemjs/plugin-css@0.1.37",
    "fetch": "github:github/fetch@2.0.4",
    "lodash": "npm:lodash@4.17.10",
    "moment": "npm:moment@2.22.2",
    "mrman/systemjs-plugin-vue-template-compiler": "gitlab:mrman/systemjs-plugin-vue-template-compiler@2.2.1",
    "mrman/vue-component-library": "gitlab:mrman/vue-component-library@1.3.4",
    "purecss": "npm:purecss@0.6.2",
    "systemjs-plugin-css": "npm:systemjs-plugin-css@0.1.37",
    "vtc": "gitlab:mrman/systemjs-plugin-vue-template-compiler@2.2.1",
    "vue": "npm:vue@2.4.0",
    "vue-component-library": "gitlab:mrman/vue-component-library@1.3.2",
    "vue-router": "npm:vue-router@2.8.1",
    "vue-template-compiler": "npm:vue-template-compiler@2.5.16",
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.4.1"
    },
    "github:jspm/nodelibs-buffer@0.1.1": {
      "buffer": "npm:buffer@5.1.0"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.10"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "github:jspm/nodelibs-vm@0.1.0": {
      "vm-browserify": "npm:vm-browserify@0.0.4"
    },
    "gitlab:mrman/systemjs-plugin-vue-template-compiler@2.2.1": {
      "vue-template-compiler": "npm:vue-template-compiler@2.4.0"
    },
    "gitlab:mrman/vue-component-library@1.3.2": {
      "css": "github:systemjs/plugin-css@0.1.37",
      "fetch": "github:github/fetch@2.0.4",
      "lodash": "npm:lodash@4.17.10",
      "moment": "npm:moment@2.22.2",
      "mrman/systemjs-plugin-vue-template-compiler": "gitlab:mrman/systemjs-plugin-vue-template-compiler@2.2.1",
      "purecss": "npm:purecss@0.6.2",
      "systemjs-plugin-css": "npm:systemjs-plugin-css@0.1.37",
      "urijs": "npm:urijs@1.19.1",
      "vue": "npm:vue@2.4.0",
      "vue-router": "npm:vue-router@2.8.1",
      "vue-template-compiler": "npm:vue-template-compiler@2.4.0"
    },
    "gitlab:mrman/vue-component-library@1.3.4": {
      "css": "github:systemjs/plugin-css@0.1.37",
      "fetch": "github:github/fetch@2.0.4",
      "lodash": "npm:lodash@4.17.10",
      "moment": "npm:moment@2.22.2",
      "mrman/systemjs-plugin-vue-template-compiler": "gitlab:mrman/systemjs-plugin-vue-template-compiler@2.2.1",
      "purecss": "npm:purecss@0.6.2",
      "systemjs-plugin-css": "npm:systemjs-plugin-css@0.1.37",
      "urijs": "npm:urijs@1.19.1",
      "vue": "npm:vue@2.4.0",
      "vue-router": "npm:vue-router@2.8.1",
      "vue-template-compiler": "npm:vue-template-compiler@2.4.0"
    },
    "npm:assert@1.4.1": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "buffer": "github:jspm/nodelibs-buffer@0.1.1",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.38": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:buffer@5.1.0": {
      "base64-js": "npm:base64-js@1.3.0",
      "ieee754": "npm:ieee754@1.1.12"
    },
    "npm:core-js@1.2.7": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.2"
    },
    "npm:de-indent@1.0.2": {
      "assert": "github:jspm/nodelibs-assert@0.1.0"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.10": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "vm": "github:jspm/nodelibs-vm@0.1.0"
    },
    "npm:purecss@0.6.2": {
      "css": "github:systemjs/plugin-css@0.1.37"
    },
    "npm:urijs@1.19.1": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:vm-browserify@0.0.4": {
      "indexof": "npm:indexof@0.0.1"
    },
    "npm:vue-router@2.8.1": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:vue-template-compiler@2.4.0": {
      "de-indent": "npm:de-indent@1.0.2",
      "he": "npm:he@1.1.1",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.2"
    },
    "npm:vue-template-compiler@2.5.16": {
      "de-indent": "npm:de-indent@1.0.2",
      "he": "npm:he@1.1.1",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.2"
    },
    "npm:vue@2.4.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    }
  }
});
